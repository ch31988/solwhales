import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { WhaleOverviewDialogComponent } from './whale-overview-dialog/whale-overview-dialog.component';
import Web3 from "web3";
import Web3Modal from "web3modal";
import { ContractService } from "./services/contract.service";
import { Whale } from "./classes/whale.class";
import { WhalesService } from './services/whales.service';

export enum WalletStatus {
  CONNECTED = 0,
  FAILED = 1,
  NO_CONNECTED = 2,
  UNKNOWN = 3
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ywhales';
  coin = 'eth';
  selectedAddress = '';
  public useDefault = false;
  public walletStatus: WalletStatus ;
  public web3 = new Web3();
  public contractService = new ContractService();
  public whales: Whale[] = [];
  public myWhales: Whale[] = [];

  public itemsPerColumn = 15;
  public whalesLoaded = false;
  public hasYPO = false;

  ngOnInit(): void {
    this.whalesService.getWhales$().subscribe(whales => {
      this.whales = whales;
    });
    this.whalesService.getMyWhales$().subscribe(myWhales => {
      console.log("myWhales: ", myWhales.length)
      const verif = myWhales.some(whale => typeof whale.whaleId === "string" && whale.whaleId === "1");
      this.hasYPO = verif ? true : false;
      this.myWhales = myWhales.sort((a,b) => (a.whaleId > b.whaleId) ? 1 : ((b.whaleId > a.whaleId) ? -1 : 0));
    });
    if(typeof window.ethereum !== 'undefined')
    {
      this.walletStatus = WalletStatus.NO_CONNECTED;
      this.openMetamask();
    }
    if(this.walletStatus !== WalletStatus.CONNECTED)
    {
      this.openMetamask();
    }
  }

  ngDoCheck(){
    if (this.walletStatus == 0) {
      this.selectedAddress = window.ethereum.selectedAddress;
    }
    if (window.ethereum.chainId === "0x3") {
      this.coin = "eth";
    }
    if (window.ethereum.chainId === "0x13881" || window.ethereum.chainId === "0x89") {
      this.coin = "matic";
    }
    if (window.ethereum.selectedAddress === null) {
      this.walletStatus = WalletStatus.NO_CONNECTED;
    }
  }

  constructor(
    public dialog: MatDialog,
    public whalesService: WhalesService
  ) {}

  public mintToggle(event: MatSlideToggleChange) {
    console.log('toggle', event.checked);
    this.useDefault = event.checked;
    if (event.checked) {
      this.openDialog(new Whale('',0,0,0,0,true,true), this.whales,false, true, true);
    }
  }

  openDialog(whale: Whale, whales: Whale[] ,mode: boolean, mint6: boolean, hasYPO: boolean): void {
    const totalNftsLeft = whales
      .filter( asset => typeof asset.whaleId === "string" && asset.whaleId !== "1")
      .reduce((totalLeft,asset) => totalLeft + Number(asset.unsoldQty), 0);
    const dialogRef = this.dialog.open(WhaleOverviewDialogComponent, {
      width: '880px',
      data: {account: this.selectedAddress, web3: this.web3, whale: whale, totalNftsLeft: totalNftsLeft, coin: this.coin, simpleMode: mode, mint6: mint6, hasYPO: hasYPO},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.useDefault = false;
    });
  }

  async openMetamask() {
    let isConnected: boolean;
    if (this.walletStatus !== WalletStatus.CONNECTED) {
      this.web3.setProvider(window.ethereum)
      try {
        // Request account access
        const connection = await window.ethereum.enable();
        this.walletStatus = WalletStatus.CONNECTED;
        this.whalesService.fetchMyTokens(
          this.walletStatus,
          this.contractService,
          this.web3,
          this.selectedAddress
        );
        this.whalesService.checkWhalesStruct(
          this.walletStatus,
          this.contractService,
          this.web3
        );
        isConnected = true;
      } catch(e) {
        // User denied access
        console.log(e);
        isConnected =  false
        WalletStatus.NO_CONNECTED;
      }
    }
  }

  async switchToMainnet() {
    window.ethereum.request({ method: 'wallet_switchEthereumChain', params: [{"chainId": "0x1",}] })
    .then(() => console.log('Success'))
    .catch((error: Error) => console.log("Error", error.message))
  }

  async switchToRopsten() {
    window.ethereum.request({ method: 'wallet_switchEthereumChain', params: [{"chainId": "0x3",}] })
    .then(() => console.log('Success'))
    .catch((error: Error) => console.log("Error", error.message))
  }

  async addNetworkPolyMain() {
    if (this.walletStatus !== 0) {
      await this.openMetamask();
    }
    const params = [{
      chainId: '0x89',
      chainName: 'Polygon Mainnet',
      nativeCurrency: {
        name: 'Polygon Mainnet',
        symbol: 'MATIC',
        decimals: 18
      },
      rpcUrls: ['https://polygon-rpc.com/'],
      blockExplorerUrls: ['https://polygonscan.com/']
    }]
    window.ethereum.request({ method: 'wallet_addEthereumChain', params })
    .then(() => console.log('Success'))
    .catch((error: Error) => console.log("Error", error.message))
  }

  addNetworkPolyMum() {
    const params = [{
      chainId: '0x13881',
      chainName: 'Polygon Testnet Mumbai',
      nativeCurrency: {
        name: 'Polygon Testnet Mumbai',
        symbol: 'MATIC',
        decimals: 18
      },
      rpcUrls: ['https://matic-mumbai.chainstacklabs.com'],
      blockExplorerUrls: ['https://mumbai.polygonscan.com/']
    }]
    window.ethereum.request({ method: 'wallet_addEthereumChain', params })
    .then(() => console.log('Success'))
    .catch((error: Error) => console.log("Error", error.message))
  }

  sortWhales(whales, hasYPO) {
    let sortedWhales = whales.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
    if (hasYPO) {
      sortedWhales = sortedWhales.filter(whale => whale.whaleId !== "1");
      return sortedWhales;
    } else {
      sortedWhales = sortedWhales.filter(whale => whale.whaleId === "1");
      return sortedWhales;
    }
  }

  calculateVisibleCells(sortedWhales) {
    const cells = Math.ceil(sortedWhales.length / this.itemsPerColumn);;
    return cells;
  }

  generateCellsArray(cells) {
    const cellsArray = new Array(cells);
    return cellsArray;
  }

}
