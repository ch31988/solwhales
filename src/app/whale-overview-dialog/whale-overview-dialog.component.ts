import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { FormControl, Validators, AbstractControl, ValidationErrors, ValidatorFn, FormGroup } from '@angular/forms';
import { Whale } from '../classes/whale.class';
import { ContractService } from "../services/contract.service";
import Web3 from 'web3';


export interface DialogData {
  account: string;
  web3: Web3;
  whale: Whale;
  totalNftsLeft: number;
  coin: string;
  simpleMode: boolean;
  mint6: boolean;
  hasYPO: boolean;
}

interface Coin {
  id: string;
  name: string;
  symbol: string;
  image: string;
  current_price: number;
}

@Component({
  selector: 'app-whale-overview-dialog',
  templateUrl: './whale-overview-dialog.component.html',
  styleUrls: ['./whale-overview-dialog.component.scss']
})
export class WhaleOverviewDialogComponent implements OnInit {

  public whaleValue = 0;
  public whaleQuantity = this.data.mint6 ? 6 : 1;
  public currentPrice = 0;
  public coin;
  public isMinting = false;
  public minted = {state: false, success: false};
  private contractService = new ContractService();


  mintForm: FormGroup;
  quantity: FormControl;
  public isValidQuantity = true;

  constructor(
    public dialogRef: MatDialogRef<WhaleOverviewDialogComponent>,
    private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.http.get<Coin[]>('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false')
      .subscribe(
        (res) => {
          this.coin = res.filter(coin => coin.symbol === this.data.coin);
          console.log(...this.coin);
          this.currentPrice = this.coin[0].current_price;
        },
        (err) => console.error(err))
   this.whaleValue = this.data.whale.price;
  }

  createForm() {
    this.quantity = new FormControl(1, [Validators.required, Validators.pattern("[1-9]+[0-9]*"), Validators.max(this.data.mint6 ? this.data.totalNftsLeft : this.data.whale.unsoldQty)]);
    this.mintForm = new FormGroup({
      quantity: this.quantity
    });
  }

  checkQuantity(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const invalidQuantity = nameRe.test(control.value);
      return invalidQuantity ? {invalidQuantity: {value: control.value}} : null;
    };
  }

  finishMinting(): void {
    this.isMinting = true;
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.mintForm.valid) {
      this.isMinting = true;
      // Here we should add the mint function call
      console.log(this.data);
      if(this.data.mint6 === false)
      {
        const mintCall = this.contractService.Mint(this.data.account, parseInt(this.quantity.value),this.data.whale.whaleId,this.data.web3, (this.whaleValue * this.quantity.value));
        mintCall.then((value) => {
          console.log("Message: ",value);
          this.minted.state = true;
          this.minted.success = true;
        }).catch((err) => {
          console.error("Error: ",err);
          this.minted.state = true;
          this.minted.success = false;
        });
      }
      else
      {
        const mint6 = this.contractService.Mint6(this.data.account, this.data.web3, 1);
        mint6.then((value) => {
          console.log("Message: ",value);
          this.minted.state = true;
          this.minted.success = true;
        })
        .catch((err) => {
          console.error("Error: ",err);
          this.minted.state = true;
          this.minted.success = false;
        });

      }
         console.log("Form Submitted!");
    }
  }

  convertPriceToValue(value: string): number {
    return parseFloat(value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
