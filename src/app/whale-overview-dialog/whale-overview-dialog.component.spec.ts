import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhaleOverviewDialogComponent } from './whale-overview-dialog.component';

describe('WhaleOverviewDialogComponent', () => {
  let component: WhaleOverviewDialogComponent;
  let fixture: ComponentFixture<WhaleOverviewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhaleOverviewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhaleOverviewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
