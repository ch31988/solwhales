import { Injectable } from '@angular/core';
import Web3 from "web3";
import Web3Modal, { local } from "web3modal";
import { Subject, } from 'rxjs';
import { ETH_contract_address, contract_abi, MATIC_contract_address, ETH_ROPSTEN_contract_address, MATIC_MUMBAI_contract_address} from "../classes/contract.class";
import { Whale } from "../classes/whale.class"
@Injectable({
  providedIn: 'root'
})

export class chainContract {
  chainContractAddress: any;
}

export class ContractService {
  public chainContractInfo: chainContract;

  constructor() {


  }

  public async WhalesStructs(web3: Web3) {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);
      const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
      const whalesStruct = await contract.methods.WhalesStructs().call();
      console.log("Whales",whalesStruct);


      return whalesStruct;
  }

  public async Whales(whaleId: number, web3: Web3) {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);
    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const whaleCall = await contract.methods.Whales(whaleId).call();
      return whaleCall;
  }

  public async Mint(account: string, quantity: number, whaleId: number, web3: Web3, value: number)
  {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);

    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const whaleMint = await contract.methods.mint(quantity,whaleId).send({ from: account, value: value});
    return whaleMint;
  }

  public async Mint6(account: string, web3: Web3, value: number)
  {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);

    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const whaleRandom = await contract.methods.mint6().send({ from: account, value: web3.utils.toWei(value.toString())});
    return whaleRandom;

  }

  public async BalanceOf(web3: Web3, account: string)
  {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);
    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const balanceOf = await contract.methods.balanceOf(account).call();
    return balanceOf;
  }

  public async TokenOfOwnerByIndex(web3: Web3, account: string, indexOf: number)
  {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);

    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const tokenOfOwnerByIndex = await contract.methods.tokenOfOwnerByIndex(account, indexOf).call();
    return tokenOfOwnerByIndex;

  }

  public async Tokens(web3: Web3, tokenId: number )
  {
    this.chainContractInfo = this.getChainContract(window.ethereum.chainId);

    const contract = new web3.eth.Contract(contract_abi as any,this.chainContractInfo.chainContractAddress);
    const tokenIdContract = await contract.methods.Tokens(tokenId).call();
    return tokenIdContract;
  }


  getChainContract(chainId: any) {

    const localChain = new chainContract();
    // ETH Mainnet
    if(chainId === '0x1') {
      localChain.chainContractAddress = ETH_contract_address;
    }
    // Matic Mainnet
    if(chainId === '0x89') {
      localChain.chainContractAddress = MATIC_contract_address;
    }
    // ETH Ropsten
    if(chainId === '0x3') {
      localChain.chainContractAddress = ETH_ROPSTEN_contract_address;
    }
    // Matic Mumbai
    if(chainId === '0x13881') {
      localChain.chainContractAddress = MATIC_MUMBAI_contract_address;
    }
    return localChain;
  }

}
