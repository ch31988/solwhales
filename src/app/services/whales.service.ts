import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Whale } from '../classes/whale.class';

export interface whaleJson {
  description: string;
  external_url: string;
  name: string;
  animation_url: string;
  attributes: []
}

@Injectable({
  providedIn: 'root'
})
export class WhalesService {

  private whales: Whale[];
  private whales$: Subject<Whale[]>;
  private myWhales: Whale[];
  private myWhales$: Subject<Whale[]>;
  public whalesLoaded = false;


  constructor(private http: HttpClient) {
    this.whales = [];
    this.whales$ = new Subject();
    this.myWhales = [];
    this.myWhales$ = new Subject();


  }


  checkWhalesStruct(walletStatus, contractService, web3) {
    if (walletStatus === 0 && !this.whalesLoaded) {
      const whalesCall = contractService.WhalesStructs(web3).then((value) => {
        for(var i = 0; i <= value; i++)
        {
          const whaleCall = contractService.Whales(i,web3).then((whaleDesc: Whale) => {
            if (whaleDesc.name !== "") {

              const whaleUrl = this.getUrl(whaleDesc.whaleId).then((value) => {
                whaleDesc.urlAsset = value;
              })
              this.whales.push(whaleDesc);
              this.whales$.next(this.whales);
            }
          })
        }
      });
      this.whalesLoaded = true;
    }
  }

  fetchMyTokens(walletStatus, contractService, web3, selectedAddress) {
    if (walletStatus == 0 && selectedAddress != '') {
      this.myWhales = [];
      const balanceOfAccount = contractService.BalanceOf(web3, selectedAddress).then((balance) => {
        if(balance > 0)
        {
          for(let i = 0; i < balance; i++)
          {
            const tokenOwnerByIndex = contractService.TokenOfOwnerByIndex(web3, selectedAddress, i).then((tokenIdWhale) => {
              const tokenId = contractService.Tokens(web3,tokenIdWhale).then((myWhaleId) => {
                const myWhale = contractService.Whales(myWhaleId,web3).then((myOwnedWhale: Whale) => {
                  const myWhaleUrl = this.getUrl(myOwnedWhale.whaleId).then((value) => {
                    myOwnedWhale.urlAsset = value;
                  });
                  this.myWhales.push(myOwnedWhale);
                  this.myWhales$.next(this.myWhales);
                });
              });

            });
          }
        }
      });
    }
  }

  async getUrl(whaleId: number) {
    const urlData = this.http.get<whaleJson>('tokens/' + whaleId + '.json').toPromise();
    return (await urlData).animation_url;
  }

  getWhales$(): Observable<Whale[]> {
    return this.whales$.asObservable()
  }

  getMyWhales$(): Observable<Whale[]> {
    return this.myWhales$.asObservable()
  }

}
