import { bool, number, string } from "prop-types";

export class Whale {

  constructor(name: string, quantity: number, unsoldQty: number, price: number, whaleId: number, transferable: boolean, saleable: boolean, urlAsset?: string) {
      this.name = name;
      this.quantity = quantity;
      this.unsoldQty = unsoldQty;
      this.price = price;
      this.whaleId = whaleId;
      this.transfereable = transferable;
      this.saleable = saleable;
      this.urlAsset = urlAsset;
  }

  public name: string;
  public quantity: number;
  public unsoldQty: number;
  public price: number;
  public whaleId: number;
  public transfereable: boolean;
  public saleable: boolean;
  public urlAsset?: string;
}
