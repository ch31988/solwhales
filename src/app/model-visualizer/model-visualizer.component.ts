import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-model-visualizer',
  templateUrl: './model-visualizer.component.html',
  styleUrls: ['./model-visualizer.component.scss']
})
export class ModelVisualizerComponent implements OnInit {

  @Input() whaleName: string;
  @Input() height: string;
  @Input() width: string;
  @Input() random: boolean;
  @Input() isCover: boolean;
  @Input() whaleUrl: string;

  public whalesRows;
  public whalesPerRow;

  constructor(private http: HttpClient) {
    this.whalesRows = Array(2).fill(0).map((x,i)=>i);
    this.whalesPerRow = Array(3).fill(0).map((x,i)=>i);
  }

  ngOnInit(): void {
  }

}
