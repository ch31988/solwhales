import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelVisualizerComponent } from './model-visualizer.component';

describe('ModelVisualizerComponent', () => {
  let component: ModelVisualizerComponent;
  let fixture: ComponentFixture<ModelVisualizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelVisualizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelVisualizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
